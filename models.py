# -*- coding: utf-8 -*-

from openerp import models, fields, api

class Hospital(models.Model):
    """ Módelo para la entidad hospital """

    _name = 'soniqsacore.hospital'

    name = fields.Char(string='Nombre', required=True)
    city_id = fields.Many2one('res.country.state.city', 'Ciudad')
    direccion = fields.Text(required=True)

class Medico(models.Model):
    ''' modelo para la clase medico '''

    _name = 'soniqsacore.medico'

    name = fields.Char(string='Nombre y Apellido', required=True)
    city_id = fields.Many2one('res.country.state.city', 'Ciudad')
    hospital_id = fields.Many2one('soniqsacore.hospital', 'Hospital')


class sale_order(models.Model):
    _inherit = 'sale.order'

    hospital_id = fields.Many2one('soniqsacore.hospital', 'Hospital', required=True)
