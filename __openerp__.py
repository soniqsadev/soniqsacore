# -*- coding: utf-8 -*-
{
    'name': "soniqsacore",

    'summary': """
        Nucleo de adaptaciones en odoo para Soniqsa""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Soniqsa",
    'website': "http://www.soniqsa.com.mx",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'templates.xml',
        'views/hospital.xml',
        'views/medico.xml',
        'views/order_inherit.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo.xml',
    ],
}
